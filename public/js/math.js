// js/math.js

define([
], function() {

	function M() {

	}

	M.prototype.Vector = function(x1, x2, y1, y2) {
		var dx = x2 - x1;
		var dy = y2 - y1;

		this.mag = Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));

		this.x = dx / this.mag;
		this.y = dy / this.mag;

	};

	M.prototype.angleToVector = function(x, y, angle) {
		console.log(angle);
		var dx = Math.cos(angle) + 10 + x;
		var dy = Math.sin(angle) + 10 + y;

		dx -= x;
		dy -= y;

		var mag = Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));

		return {
			x: dx / mag,
			y: dy / mag
		};
	};

	return new M();
});