// js/turret.js

define([
	'phaser',
	'GS',
	'player',
	'asteroid',
	'laser'
], function(Phaser, GS, Player, Asteroid, Laser) {

	var Turret = function(game, x, y, color) {
		Phaser.Sprite.call(this, game, x, y, 'sprites', 'turretBase_big.png');
		this.anchor.set(0.5);
		this.color = color;
		this.gun_ready = true;

		if (color == 'blue') {
			this.enemy = 'green';
			this.laserCollisionGroup = game.blueLasersCollisionGroup;
			this.enemyCollisionGroup = game.greenCollisionGroup;
		}
		else if (color == 'green') {
			this.enemy = 'blue';
			this.laserCollisionGroup = game.greenLasersCollisionGroup;
			this.enemyCollisionGroup = game.blueCollisionGroup;
		}

		this.cannon = new Phaser.Image(game, 0, 0, 'sprites', 'gun06.png');
		this.cannon.anchor.set(0.5, -0.25);
		// this.cannon.angle = 180;
		this.addChild(this.cannon);

		game.physics.p2.enable(this, false);		// Set second parameter to "true" to debug physics body

		this.body.clearShapes();
		this.body.addCircle(20);

		this.body.static = true;
	};

	Turret.prototype = Object.create(Phaser.Sprite.prototype);

	Turret.prototype.constructor = Turret;

	Turret.prototype.update = function() {
		if (this.alive === true) {
			this.game[this.enemy].forEach(function(color_obj, index) {
				if (color_obj instanceof Player) {
					// If enemy gets within a certain distance of the turret, start tracking enemy
					if (Phaser.Math.distance(this.x, this.y, color_obj.x, color_obj.y) < 600) {
						this.cannon.rotation = Phaser.Math.angleBetweenPoints(color_obj.position, this.position) + 1.57;

						if (this.gun_ready === true) {

							var laser = new Laser(this);

							this.game.lasers.add(laser);

							// var laser = this.game.lasers.create(this.x, this.y, 'sprites', 'laserGreen03.png');
							// laser.body.rotation = this.cannon.rotation;
							// laser.body.velocity.x = Math.cos(this.cannon.rotation + 1.57) * 1000;
							// laser.body.velocity.y = Math.sin(this.cannon.rotation + 1.57) * 1000;
							//
							// laser.body.onBeginContact.add(function(laser_body) {
				    	// 	// laser.destroy();
							// 	// if (laser_body !== null && typeof laser_body.sprite !== 'undefined') {
							// 	// 	if (!(laser_body.sprite instanceof Asteroid)) {
							// 	// 		console.log(laser_body.sprite);
							// 	// 		laser_body.sprite.kill();
							// 	// 	}
							// 	// }
				    	// });
							//
							// laser.body.setCollisionGroup(this.laserCollisionGroup);
							// laser.body.collides([
							// 	this.enemyCollisionGroup
							// ]);

							this.gun_ready = false;
							this.game.time.events.add(Phaser.Timer.SECOND / 5, function() {
				    		this.gun_ready = true;
				    	}, this);
						}
					}
				}
			}, this);
			// this.cannon.rotation += 0.005;
		}
	};

	return Turret;

});
