// js/drone.js

define([
	'phaser',
	'GS',
	'asteroid'
], function(Phaser, GS, Asteroid) {

	var Drone = function(game, x, y, color, route, type) {

    var drones = {
			blue: {
				'1': {
					sprite: 'enemyBlue1.png',
					physics: 'enemy1'
				},
				'2': {
					sprite: 'enemyBlue2.png',
					physics: 'enemy2'
				},
				'3': {
					sprite: 'enemyBlue3.png',
					physics: 'enemy3'
				},
				'4': {
					sprite: 'enemyBlue4.png',
					physics: 'enemy4'
				}
			},
			green: {
				'1': {
					sprite: 'enemyGreen1.png',
					physics: 'enemy1'
				},
				'2': {
					sprite: 'enemyGreen2.png',
					physics: 'enemy2'
				},
				'3': {
					sprite: 'enemyGreen3.png',
					physics: 'enemy3'
				},
				'4': {
					sprite: 'enemyGreen4.png',
					physics: 'enemy4'
				}
			}
		};

		if (typeof type === 'undefined') {
			var types = ['1', '2', '3', '4'];
			type = types[Math.floor(Math.random() * types.length)];
		}

		Phaser.Sprite.call(this, game, x, y, 'sprites', drones[color][type].sprite);
		this.anchor.set(0.5);

		this.health = 50;

		// Values for target force calculation
		this.engage_distance = 300;
		this.speed_limit = 200;

		this.gun_ready = true;

		this.color = color;
		if (this.color == 'blue') {
			this.enemy_color = 'green';
			this.laser_color = 'laserBlue03.png';
			this.laserCollisionGroup = game.blueLasersCollisionGroup;
			this.enemyCollisionGroup = game.greenCollisionGroup;
		}
		else {
			this.enemy_color = 'blue';
			this.laser_color = 'laserGreen03.png';
			this.laserCollisionGroup = game.greenLasersCollisionGroup;
			this.enemyCollisionGroup = game.blueCollisionGroup;
		}

		this.route = route;
		this.targets = [];

		// Build target array for route
		this.turrets = game.map.getObjectsByType(this.enemy_color, 'turret');

		if (typeof this.turrets !== 'undefined') {
			this.turrets.forEach(function(turret, index) {
				var turret_route = turret.name.split('_');
				if (turret_route[0] == this.route) {
					this.targets[parseInt(turret_route[1])] = turret;
				}
			}, this);
		}

		this.targets.push(this.game.map.getObjectsByName(this.enemy_color, 'base'));
		this.targets.shift();

		this.target = this.targets[0];

		this.thrust = 175;

		game.physics.p2.enable(this, false);		// Set second parameter to "true" to debug physics body

		this.body.clearShapes();
		this.body.loadPolygon('sprite_physics', drones[color][type].physics);
		this.body.mass = 3;

		if (this.route == 'top') {
			this.body.angle = 180;
		}
		else if (this.route == 'middle') {
			this.body.angle = 225;
		}
		else if (this.route == 'bottom') {
			this.body.angle = 270;
		}
	};

	Drone.prototype = Object.create(Phaser.Sprite.prototype);

	Drone.prototype.constructor = Drone;

	Drone.prototype.update = function() {

		if (this.alive === true) {
			this.body.rotation = Phaser.Math.angleBetween(this.x, this.y, this.target.x, this.target.y) - 1.57;

			if (this.target.type == 'turret' || this.target.name == 'base') {
				this.game[this.enemy_color].forEach(function(enemy, index) {
					if (Phaser.Math.distance(this.x, this.y, enemy.x, enemy.y) < 300) {
						this.target = enemy;
					}
				}, this);
			}
			else if (this.target.alive === false) {
				// console.log(this.targets[0]);
				if (this.targets[0].alive === false) {
					this.targets.shift();
					// console.log(this.targets);
				}
				this.target = this.targets[0];
			}
			else {
				if (this.gun_ready === true) {
					var laser = this.game.lasers.create(this.x, this.y, 'sprites', this.laser_color);
					laser.body.setCollisionGroup(this.laserCollisionGroup);
					laser.body.collides([
						this.enemyCollisionGroup,
						this.game.commonCollisionGroup
					]);

					laser.body.rotation = this.body.rotation;
					laser.body.velocity.x = (Math.cos(this.body.rotation + 1.57) * 1000) + this.body.velocity.x;
					laser.body.velocity.y = (Math.sin(this.body.rotation + 1.57) * 1000) + this.body.velocity.y;
					laser.body.mass = 0.01;
					laser.body.onBeginContact.add(function(laser_body) {
						// laser.destroy();
						// if (laser_body !== null && typeof laser_body.sprite !== 'undefined') {
						// 	if (!(laser_body.sprite instanceof Asteroid)) {
						// 		laser_body.sprite.kill();
						// 	}
						// }
					});

					this.gun_ready = false;
					this.game.time.events.add(Phaser.Timer.SECOND / 2, function() {
						this.gun_ready = true;
					}, this);
				}
			}

			// Check for proximity to checkpoint
			if (Phaser.Math.distance(this.x, this.y, this.target.x, this.target.y) > this.engage_distance) {
				if (Math.abs(this.body.velocity.x) < this.speed_limit) {
					this.body.force.x = Math.cos(this.body.rotation + 1.57) * this.thrust;
				}

				if (Math.abs(this.body.velocity.y) < this.speed_limit) {
					this.body.force.y = Math.sin(this.body.rotation + 1.57) * this.thrust;
				}
			}
			else {
				// Checkpoint reached, move towards enemy base

				// if (this.target.type == 'checkpoint') {
				// 	if (this.route == 'top' || this.route == 'bottom') {
				// 		this.checkpoints.forEach(function(checkpoint, index) {
				// 			if (checkpoint.name == 'middle') {
				// 				this.target = checkpoint;
				// 			}
				// 		}, this);
				// 	}
				// }
				// else {
				//
				// }

			}
		}
	};

	return Drone;

});
