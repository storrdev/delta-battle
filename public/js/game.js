// js/app.js

define([
	'phaser',
	'GS',
	'math',
	'player',
	'ship',
	'laser',
	'asteroid',
	'turret',
	'drone'
], function(Phaser, GS, M, Player, Ship, Laser, Asteroid, Turret, Drone) {
	'use strict';

	function Game() {
		console.log('Making the Game!');

		/*
		*		Extend native Phaser prototypes
		*/

		Phaser.Tilemap.prototype.getObjectsByName = function(layer, name) {
			var matches = [];

			if (typeof this.objects[layer] !== 'undefined') {
				this.objects[layer].forEach(function(object, index) {
					if (object.name == name) {
						matches.push(object);
					}
				});
			}

			if (matches.length === 0) {
				matches = false;
			}
			// else if (matches.length == 1) {
			// 	matches = matches[0];
			// }

			return matches;
		};

		Phaser.Tilemap.prototype.getObjectsByType = function (layer, type) {
			var matches = [];

			this.objects[layer].forEach(function(object, index) {
				if (object.type == type) {
					matches.push(object);
				}
			});

			if (matches.length === 0) {
				matches = false;
			}
			// else if (matches.length == 1) {
			// 	matches = matches[0];
			// }

			return matches;
		};
	}

	Game.prototype = {
		constructor: Game,

		start: function() {
			this.game = new Phaser.Game(window.innerWidth, window.innerHeight, Phaser.AUTO, '', {
				preload: this.preload,
				create: this.create,
				update: this.update,
				render: this.render
			});
		},

		preload: function() {
			// Keeps game rendering when window loses focus
			// !Important for multiplayer testing
			this.game.stage.disableVisibilityChange = true;

			// Load tilesprite background images
			this.game.load.image('background', 'assets/backgrounds/background.png');

			// Load the Tiled json map file
			// this.game.load.tilemap('map', 'assets/maps/original/original.json', null, Phaser.Tilemap.TILED_JSON);
			this.game.load.tilemap('map', 'assets/maps/tests/turret_test.json', null, Phaser.Tilemap.TILED_JSON);

			// Load spritesheet
			this.game.load.atlasJSONHash('sprites', 'assets/sprites/sprites.png', 'assets/sprites/sprites.json');
			// this.game.load.image('sprites', 'assets/sprites/sprites.png');

			// Load sprite physics from Physics Editor
			this.game.load.physics('sprite_physics', 'assets/sprites/sprite_physics.json');
		},

		create: function() {
			var Game = this;

			GS.connect(function() {});

			// GS.on('all players', function(players) {
			// 	// console.log(players);

			// 	players.forEach(function(playerObj, index){
			// 		var player = new Player(this.game, 0, 0);
			// 		this.players.push
			// 	});
			// });

			GS.on('new player', function(conn) {
				console.log(conn);

				var player = new Player(Game, 200, 200, conn);
				Game.add.existing(player);
			});

			GS.on('peer data', function(data) {
				// console.log(peerId);

				if (data.type == 'player') {
					Game.world.children.forEach(function(child, index){
						if (child instanceof Player && typeof child.conn != 'undefined') {
							if (child.conn.peerId == peerId) {
								child.reset(data.x, data.y);
								child.body.velocity.x = data.vX;
								child.body.velocity.y = data.vY;
								child.body.angle = data.angle;
							}
						}
					});
				}
				else if (data.type == 'laser') {

				}

			});

			var that = this;

			this.game.map = this.game.add.tilemap('map');

			this.game.world.setBounds(0, 0, (this.game.map.width * this.game.map.tileWidth), (this.game.map.height * this.game.map.tileHeight));

			this.game.physics.startSystem(Phaser.Physics.P2JS);
			this.game.physics.p2.setImpactEvents(true);
			this.game.physics.p2.defaultRestitution = 0.8;

			this.game.blueCollisionGroup = this.game.physics.p2.createCollisionGroup();
			this.game.blueLasersCollisionGroup = this.game.physics.p2.createCollisionGroup();
			this.game.greenCollisionGroup = this.game.physics.p2.createCollisionGroup();
			this.game.greenLasersCollisionGroup = this.game.physics.p2.createCollisionGroup();
			this.game.commonCollisionGroup = this.game.physics.p2.createCollisionGroup();

			//  This part is vital if you want the objects with their own collision groups to still collide with the world bounds
			//  (which we do) - what this does is adjust the bounds to use its own collision group.
			this.game.physics.p2.updateBoundsCollisionGroup();

			this.background = this.game.add.tileSprite(0, 0, this.game.width, this.game.height, 'background');
			this.background.fixedToCamera= true;

			/*
			*		Sprite Groups
			*/

			// Blue team
			this.game.blue = this.game.add.group();
			this.game.blue.enableBody = true;
			this.game.blue.physicsBodyType = Phaser.Physics.P2JS;

			// Green team
			this.game.green = this.game.add.group();
			this.game.green.enableBody = true;
			this.game.green.physicsBodyType = Phaser.Physics.P2JS;

			// Bases array
			// For use with asteroid 'collection' detection
			// This optimizes the distance calculation between asteroids and bases
			this.game.bases = [];

			// Lasers
			this.game.lasers = this.game.add.group();
			this.game.lasers.enableBody = true;
			this.game.lasers.physicsBodyType = Phaser.Physics.P2JS;

			// Asteroids
			this.game.asteroids = this.game.add.group();
			this.game.asteroids.enableBody = true;
			this.game.asteroids.physicsBodyType = Phaser.Physics.P2JS;

			/*
			*		Map Stuff
			*/

			this.game.map.createFromObjects('blue', 'base', 'sprites', 'ufoBlue.png', true, false, this.game.blue);
			this.game.map.createFromObjects('green', 'base', 'sprites', 'ufoGreen.png', true, false, this.game.green);

			this.game.blue.forEach(function(blue_object, index) {
				if (blue_object.name == 'base') {
					blue_object.body.clearShapes();
					blue_object.body.addCircle(45);
					// blue_object.body.debug = true;
					blue_object.anchor.set(0.5);
					this.game.bases.push(blue_object);
				}
				blue_object.body.static = blue_object.static;
				blue_object.body.setCollisionGroup(this.game.blueCollisionGroup);
				blue_object.body.collides([
					this.game.blueCollisionGroup,
					this.game.greenCollisionGroup,
					this.game.greenLasersCollisionGroup
				]);
			}, this);

			this.game.green.forEach(function(green_object, index) {
				if (green_object.name == 'base') {
					green_object.body.clearShapes();
					green_object.body.addCircle(45);
					// green_object.body.debug = true;
					green_object.anchor.set(0.5);
					this.game.bases.push(green_object);
				}
				green_object.body.setCollisionGroup(this.game.greenCollisionGroup);
				green_object.body.collides([
					this.game.greenCollisionGroup,
					this.game.blueCollisionGroup,
					this.game.blueLasersCollisionGroup
				]);
				green_object.body.static = green_object.static;
			}, this);

			/*
			*		Spawn Turrets
			*/

			var blue_turret_spawn_objs = this.game.map.getObjectsByType('blue', 'turret');

			if (blue_turret_spawn_objs) {
				blue_turret_spawn_objs.forEach(function(turret_obj, index) {
					var turret = new Turret(this.game, turret_obj.x, turret_obj.y, 'blue');
					this.game.blue.add(turret);

					turret.body.setCollisionGroup(this.game.blueCollisionGroup);
					turret.body.collides([
						this.game.blueCollisionGroup,
						this.game.greenCollisionGroup,
						this.game.greenLasersCollisionGroup,
						this.game.commonCollisionGroup
					]);
				}, this);
			}

			var green_turret_spawn_objs = this.game.map.getObjectsByType('green', 'turret');

			if (green_turret_spawn_objs) {
				green_turret_spawn_objs.forEach(function(turret_obj, index) {
					var turret = new Turret(this.game, turret_obj.x, turret_obj.y, 'green');
					this.game.green.add(turret);

					turret.body.setCollisionGroup(this.game.greenCollisionGroup);
					turret.body.collides([
						this.game.greenCollisionGroup,
						this.game.blueCollisionGroup,
						this.game.blueLasersCollisionGroup,
						this.game.commonCollisionGroup
					]);
				}, this);
			}

			/*
			*		Add Players
			*/

			var blue_player_spawns = this.game.map.getObjectsByType('blue', 'spawn');

			blue_player_spawns.forEach(function(spawn, index) {
				var player_spawn = {
					x: spawn.x,
					y: spawn.y
				};

				this.player = new Player(this.game, player_spawn.x, player_spawn.y, 'blue');
				this.game.blue.add(this.player);

				this.player.body.setCollisionGroup(this.game.blueCollisionGroup);
				this.player.body.collides([
					this.game.blueCollisionGroup,
					this.game.greenCollisionGroup,
					this.game.greenLasersCollisionGroup,
					this.game.commonCollisionGroup
				]);

				this.game.camera.follow(this.player);
			}, this);

			/*
			*		Add Drones
			*/

			var drone_spawns = this.game.map.getObjectsByType('blue', 'drones');

			if (drone_spawns) {
				drone_spawns.forEach(function(spawn, index) {
					for (var i = 0; i < 3; i++) {

						// Check position of already spawn asteroids, until a free area is found, then spawn there.
						var spawn_available = false;
						var x = (spawn.x + spawn.width) / 2;
						var y = (spawn.y + spawn.height) / 2;

						while (!spawn_available) {
							// Generate random coords to spawn on
							x = Math.floor(Math.random() * ((spawn.x + spawn.width) - spawn.x)) + spawn.x;
							y = Math.floor(Math.random() * ((spawn.y + spawn.height) - spawn.y)) + spawn.y;

							var clear = true;

							for(var m = 0; m < this.game.blue.length; m++) {
								var blue_object = this.game.blue.getChildAt(m);

								if (Phaser.Math.distance(x, y, blue_object.x, blue_object.y) < 40) {
									console.log('drone spawn too close to blue object, trying again');
									clear = false;
									break;
								}
							}
							spawn_available = clear;
						}

						var drone = new Drone(this.game, x, y, 'blue', spawn.name);

						this.game.blue.add(drone);

						drone.body.setCollisionGroup(this.game.blueCollisionGroup);
						drone.body.collides([
							this.game.blueCollisionGroup,
							this.game.greenCollisionGroup,
							this.game.greenLasersCollisionGroup
						]);
					}
				}, this);
			}

			drone_spawns = this.game.map.getObjectsByType('green', 'drones');

			if (drone_spawns) {
				drone_spawns.forEach(function(spawn, index) {
					for (var i = 0; i < 3; i++) {

						// Check position of already spawn asteroids, until a free area is found, then spawn there.
						var spawn_available = false;
						var x = (spawn.x + spawn.width) / 2;
						var y = (spawn.y + spawn.height) / 2;

						while (!spawn_available) {
							// Generate random coords to spawn on
							x = Math.floor(Math.random() * ((spawn.x + spawn.width) - spawn.x)) + spawn.x;
							y = Math.floor(Math.random() * ((spawn.y + spawn.height) - spawn.y)) + spawn.y;

							var clear = true;

							for(var m = 0; m < this.game.green.length; m++) {
								var green_object = this.game.green.getChildAt(m);

								if (Phaser.Math.distance(x, y, green_object.x, green_object.y) < 40) {
									console.log('drone spawn too close to blue object, trying again');
									clear = false;
									break;
								}
							}
							spawn_available = clear;
						}

						var drone = new Drone(this.game, x, y, 'green', spawn.name);

						this.game.green.add(drone);

						drone.body.setCollisionGroup(this.game.greenCollisionGroup);
						drone.body.collides([
							this.game.greenCollisionGroup,
							this.game.blueCollisionGroup,
							this.game.blueLasersCollisionGroup
						]);
					}
				}, this);
			}

			/*
			*		Spawn Asteroids
			*/

			// Area on tiled map that shows where to spawn asteroids
			var asteroid_spawn_obj = this.game.map.getObjectsByName('asteroids', 'spawn_area');
			var asteroid;

			if (asteroid_spawn_obj) {
				for (var i = 0; i < 50; i++) {

					// Check position of already spawn asteroids, until a free area is found, then spawn there.
					var spawn_available = false;
					var x = (asteroid_spawn_obj.x + asteroid_spawn_obj.width) / 2;
					var y = (asteroid_spawn_obj.y + asteroid_spawn_obj.height) / 2;

					while (!spawn_available) {
						// Generate random coords to spawn on
						x = Math.floor(Math.random() * ((asteroid_spawn_obj.x + asteroid_spawn_obj.width) - asteroid_spawn_obj.x)) + asteroid_spawn_obj.x;
						y = Math.floor(Math.random() * ((asteroid_spawn_obj.y + asteroid_spawn_obj.height) - asteroid_spawn_obj.y)) + asteroid_spawn_obj.y;

						var clear = true;

						for(var m = 0; m < this.game.asteroids.length; m++) {
							asteroid = this.game.asteroids.getChildAt(m);

							if (Phaser.Math.distance(x, y, asteroid.x, asteroid.y) < 100) {
								console.log('asteroid spawn too close, trying again');
								clear = false;
								break;
							}
						}
						spawn_available = clear;
					}

					asteroid = new Asteroid(this.game, x, y);

					this.game.asteroids.add(asteroid);

					asteroid.body.setCollisionGroup(this.game.commonCollisionGroup);
					asteroid.body.collides([
						this.game.commonCollisionGroup,
						this.game.blueCollisionGroup,
						this.game.blueLasersCollisionGroup,
						this.game.greenCollisionGroup,
						this.game.greenLasersCollisionGroup
					]);
				}
			}

			this.keys = {
				UP: this.game.input.keyboard.addKey(Phaser.Keyboard.W),
				DOWN: this.game.input.keyboard.addKey(Phaser.Keyboard.S),
				LEFT: this.game.input.keyboard.addKey(Phaser.Keyboard.A),
				RIGHT: this.game.input.keyboard.addKey(Phaser.Keyboard.D),
				TAB: this.game.input.keyboard.addKey(Phaser.Keyboard.TAB)
			};

			this.playerMenu = this.game.add.group();
			this.playerMenu.fixedToCamera = true;
			this.playerMenu.cameraOffset.setTo(0, 0);
			this.playerMenu.visible = false;

			var menuBackground = new Phaser.Graphics(this.game, 0, 0);
			menuBackground.beginFill(0x000000, 0.5);
			menuBackground.drawRect(0, 0, this.game.width, this.game.height);
			menuBackground.endFill();

			this.playerMenu.add(menuBackground);

			var style = { font: "16px Arial", fill: "#fff", tabs: [ 164, 120, 80 ] };
			var headings = ['Player', 'PeerId', 'SocketId'];

			var playerMenuHeader = new Phaser.Text(this.game, this.game.width/2, 125, '', style);
			playerMenuHeader.anchor.set(0.5);
			playerMenuHeader.parseList(headings);

			this.playerMenu.add(playerMenuHeader);

			this.game.input.mouse.capture = true;

			this.gun_ready = true;
		},

		update: function() {

			if (this.keys.TAB.isDown) {
				this.playerMenu.visible = true;
				// console.log(this.game.state.states);
			}
			else {
				this.playerMenu.visible = false;
			}

			// Slowly rotate bases
			this.game.bases.forEach(function(base, index) {
				base.body.rotation += 0.005;
			});

			// console.log(this.players)

			if (this.player.alive === true) {
				this.player.body.rotation = Phaser.Math.angleBetweenPoints(this.player.worldPosition, this.game.input.mousePointer.position) + Phaser.Math.degToRad(90);

				if (this.keys.LEFT.isDown) {
						this.player.body.velocity.x += Math.sin(Phaser.Math.angleBetweenPoints(this.player.worldPosition, this.game.input.mousePointer.position)) * this.player.speed;
						this.player.body.velocity.y += Math.cos(Phaser.Math.angleBetweenPoints(this.player.worldPosition, this.game.input.mousePointer.position)) * -this.player.speed;
				}
				else if (this.keys.RIGHT.isDown) {
						this.player.body.velocity.x += Math.sin(Phaser.Math.angleBetweenPoints(this.player.worldPosition, this.game.input.mousePointer.position)) * -this.player.speed;
						this.player.body.velocity.y += Math.cos(Phaser.Math.angleBetweenPoints(this.player.worldPosition, this.game.input.mousePointer.position)) * this.player.speed;
				}

				if (this.keys.UP.isDown) {
						this.player.body.velocity.x += Math.cos(Phaser.Math.angleBetweenPoints(this.player.worldPosition, this.game.input.mousePointer.position)) * this.player.speed;
						this.player.body.velocity.y += Math.sin(Phaser.Math.angleBetweenPoints(this.player.worldPosition, this.game.input.mousePointer.position)) * this.player.speed;
						this.player.flame.visible = true;
				}
				else {
						this.player.flame.visible = false;
				}

				if (this.keys.DOWN.isDown) {
						this.player.body.velocity.x += Math.cos(Phaser.Math.angleBetweenPoints(this.player.worldPosition, this.game.input.mousePointer.position)) * -this.player.speed;
						this.player.body.velocity.y += Math.sin(Phaser.Math.angleBetweenPoints(this.player.worldPosition, this.game.input.mousePointer.position)) * -this.player.speed;
				}
			}
			else {
				this.player.body.velocity.x = 0;
				this.player.body.velocity.y = 0;
			}

			if (!this.game.camera.atLimit.x) {
					this.background.tilePosition.x -= (this.player.body.velocity.x * this.game.time.physicsElapsed);
			}

			if (!this.game.camera.atLimit.y) {
					this.background.tilePosition.y -= (this.player.body.velocity.y * this.game.time.physicsElapsed);
			}

			// Asteroid updates
			this.game.asteroids.forEach(function(asteroid, index) {
				asteroid.update();
			});

			// Green team updates
			this.game.green.forEach(function(green, index) {
				if (typeof green.update == 'function') {
					green.update();
				}
			}, this);

			// Blue team updates
			this.game.blue.forEach(function(blue, index) {
				if (typeof blue.update == 'function') {
					blue.update();
				}
			}, this);
		},

		render: function() {
			// this.game.debug.spriteInfo(this.player, 32, 32);
			this.game.debug.body(this.player);
		}
	};

	return Game;
});
