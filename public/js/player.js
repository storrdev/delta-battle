// js/player.js

define([
	'phaser',
	'GS',
	'ship',
	'drone',
	'asteroid',
	'laser'
], function(Phaser, GS, Ship, Drone, Asteroid, Laser) {

	// "Constructor"

	var Player = function(game, x, y, color, conn) {

		this._game = game;
		this.color = color;
		this.conn = conn;
		this.gun_ready = true;

		if (this.color == 'blue') {
			this.enemy = 'green';
			this.laserCollisionGroup = this._game.blueLasersCollisionGroup;
			this.enemyCollisionGroup = this._game.greenCollisionGroup;
		}
		else {
			this.enemy = 'blue';
			this.laserCollisionGroup = this._game.greenLasersCollisionGroup;
			this.enemyCollisionGroup = this._game.blueCollisionGroup;
		}

		Ship.call(this, game, x, y);

		this.body.onBeginContact.add(function(body, shapeA, shapeB, equation) {
			if (body) {
				// console.log(body);
				if (body.sprite instanceof Laser) {
					// console.log(body.sprite.color);
					if (body.sprite.color == this.enemy) {
						// console.log(this.damage);
						this.damage();
					}
					body.sprite.destroy();
				}
			}
		}, this);
	};

	Player.prototype = Object.create(Phaser.Sprite.prototype);

	Player.prototype.constructor = Player;

	// Methods

	Player.prototype.damage = function() {
		this.health -= 1;

		console.log(this.health);

		if (this.health <= 0) {
			this.kill();

			return true;
		}

		return false;
	};

	Player.prototype.update = function() {
		// Only send peer data if the player is local (aka no connection data)
		if (typeof this.conn == 'undefined') {
			if (this.alive === true) {
				if (this._game.input.activePointer.leftButton.isDown) {
		    	if (this.gun_ready === true) {
			    	var player_angle = Phaser.Math.angleBetweenPoints(this.worldPosition, this.game.input.mousePointer.position);
						// console.log(player_angle);
						var right_gun = player_angle + Phaser.Math.degToRad(30);
			    	var left_gun = player_angle - Phaser.Math.degToRad(30);
			    	var right_x = Math.cos(right_gun) * (this.height * 0.8) + this.x;
			    	var right_y = Math.sin(right_gun) * (this.height * 0.8) + this.y;
			    	var left_x = Math.cos(left_gun) * (this.height * 0.8) + this.x;
			    	var left_y = Math.sin(left_gun) * (this.height * 0.8) + this.y;

			    	var right_laser = this._game.lasers.create(right_x, right_y, 'sprites', 'laserBlue03.png');
			    	var left_laser = this._game.lasers.create(left_x, left_y, 'sprites', 'laserBlue03.png');

						right_laser.body.setCollisionGroup(this.laserCollisionGroup);
						right_laser.body.collides([
							this.enemyCollisionGroup,
							this._game.commonCollisionGroup
						]);

						// right_laser.body.debug = true;
						right_laser.body.rotation = this.body.rotation;
			    	right_laser.body.velocity.x = (Math.cos(player_angle) * 1000) + this.body.velocity.x;
			    	right_laser.body.velocity.y = (Math.sin(player_angle) * 1000) + this.body.velocity.y;
			    	right_laser.body.mass = 0.01;
			    	right_laser.body.onBeginContact.add(function(right_laser_body, contact_body, right_laser_shape, contact_shape, equation) {
			    		right_laser.destroy();
							if (right_laser_body !== null && typeof right_laser_body.sprite !== 'undefined') {
								if (!(right_laser_body.sprite instanceof Asteroid)) {
									right_laser_body.sprite.kill();
								}
							}
			    	}, this);

						left_laser.body.setCollisionGroup(this.laserCollisionGroup);
						left_laser.body.collides([
							this.enemyCollisionGroup,
							this._game.commonCollisionGroup
						]);

						// left_laser.body.debug = true;
			    	left_laser.body.rotation = this.body.rotation;
			    	left_laser.body.velocity.x = (Math.cos(player_angle) * 1000) + this.body.velocity.x;
			    	left_laser.body.velocity.y = (Math.sin(player_angle) * 1000) + this.body.velocity.y;
			    	left_laser.body.mass = 0.01;
			    	left_laser.body.onBeginContact.add(function() {
			    		left_laser.destroy();
			    	});

			    	// GS.send({

			    	// });


			    	this.gun_ready = false;
			    	this.game.time.events.add(Phaser.Timer.SECOND / 7, function() {
			    		this.gun_ready = true;
			    	}, this);
		    	}
		    }
			}

			// console.log(this.x);
			GS.send({
				type: 'player',
				x: this.x,
				y: this.y,
				vX: this.body.velocity.x,
				vY: this.body.velocity.y,
				angle: this.angle
			});
		}
		else {

		}
	};

	return Player;
});
