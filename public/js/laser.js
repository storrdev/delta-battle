// js/laser.js

define([
	'phaser',
	'GS'
], function(Phaser, GS) {

	var Laser = function(parent) {

		this.game = parent.game;
		this.color = parent.color;

		if (this.color == 'blue') {
			this.laser_color = 'laserBlue03.png';
			this.enemy = 'green';
			this.laserCollisionGroup = this.game.blueLasersCollisionGroup;
			this.enemyCollisionGroup = this.game.greenCollisionGroup;
		}
		else {
			this.laser_color = 'laserGreen03.png';
			this.enemy = 'blue';
			this.laserCollisionGroup = this.game.greenLasersCollisionGroup;
			this.enemyCollisionGroup = this.game.blueCollisionGroup;
		}

		Phaser.Sprite.call(this, parent.game, parent.x, parent.y, 'sprites', this.laser_color);

		this.game.physics.p2.enable(this, false);

		this.body.rotation = parent.cannon.rotation;
		this.body.velocity.x = Math.cos(this.body.rotation + 1.57) * 1000;
		this.body.velocity.y = Math.sin(this.body.rotation + 1.57) * 1000;

		this.body.setCollisionGroup(this.laserCollisionGroup);
		this.body.collides([
			this.enemyCollisionGroup
		]);

		this.body.onBeginContact.add(function(body) {
			if (body === null) {	// So far only way to test for collision with world bounds
				this.destroy();
			}
		}, this);
	};

	Laser.prototype = Object.create(Phaser.Sprite.prototype);

	Laser.prototype.constructor = Laser;

	return Laser;
});
