// js/asteroid.js

define([
	'phaser',
	'GS'
], function(Phaser, GS) {

	var Asteroid = function(game, x, y, color, size) {

    var asteroids = {
			brown: {
				big1:	{
					sprite: 'asteroidBrown_big1.png',
					physics: 'asteroid_big1'
				},
				big2:	{
					sprite: 'asteroidBrown_big2.png',
					physics: 'asteroid_big2'
				},
				big3:	{
					sprite: 'asteroidBrown_big3.png',
					physics: 'asteroid_big3'
				},
				big4:	{
					sprite: 'asteroidBrown_big4.png',
					physics: 'asteroid_big4'
				},
				med1:	{
					sprite: 'asteroidBrown_med1.png',
					physics: 'asteroid_med1'
				},
				med2:	{
					sprite: 'asteroidBrown_med2.png',
					physics: 'asteroid_med2'
				},
				small1:	{
					sprite: 'asteroidBrown_small1.png',
					physics: 'asteroid_small1'
				},
				small2:	{
					sprite: 'asteroidBrown_small2.png',
					physics: 'asteroid_small2'
				},
				tiny1:	{
					sprite: 'asteroidBrown_tiny1.png',
					physics: 'asteroid_tiny1'
				},
				tiny2:	{
					sprite: 'asteroidBrown_tiny2.png',
					physics: 'asteroid_tiny2'
				}
			},
			grey: {
				big1:	{
					sprite: 'asteroidGrey_big1.png',
					physics: 'asteroid_big1'
				},
				big2:	{
					sprite: 'asteroidGrey_big2.png',
					physics: 'asteroid_big2'
				},
				big3:	{
					sprite: 'asteroidGrey_big3.png',
					physics: 'asteroid_big3'
				},
				big4:	{
					sprite: 'asteroidGrey_big4.png',
					physics: 'asteroid_big4'
				},
				med1:	{
					sprite: 'asteroidGrey_med1.png',
					physics: 'asteroid_med1'
				},
				med2:	{
					sprite: 'asteroidGrey_med2.png',
					physics: 'asteroid_med2'
				},
				small1:	{
					sprite: 'asteroidGrey_small1.png',
					physics: 'asteroid_small1'
				},
				small2:	{
					sprite: 'asteroidGrey_small2.png',
					physics: 'asteroid_small2'
				},
				tiny1:	{
					sprite: 'asteroidGrey_tiny1.png',
					physics: 'asteroid_tiny1'
				},
				tiny2:	{
					sprite: 'asteroidGrey_tiny2.png',
					physics: 'asteroid_tiny2'
				}
			}
		};

		if (typeof color === 'undefined') {
			var colors = ['brown', 'grey'];
			color = colors[Math.floor(Math.random() * colors.length)];
		}

		if (typeof size === 'undefined') {
			var sizes = [
				'big1',
				'big2',
				'big3',
				'big4',
				'med1',
				'med2',
				'small1',
				'small2',
				'tiny1',
				'tiny2'
			];
			size = sizes[Math.floor(Math.random() * sizes.length)];
		}

		Phaser.Sprite.call(this, game, x, y, 'sprites', asteroids[color][size].sprite);
		this.anchor.set(0.5);

		game.physics.p2.enable(this, false);		// Set second parameter to "true" to debug physics body

		this.body.clearShapes();
		this.body.loadPolygon('sprite_physics', asteroids[color][size].physics);
		this.body.mass = 20;
		//
		// this.speed = 3;
	};

	Asteroid.prototype = Object.create(Phaser.Sprite.prototype);

	Asteroid.prototype.constructor = Asteroid;

	Asteroid.prototype.update = function() {
		// Check for proximity to bases
		this.game.bases.forEach(function(base, bindex) {
			var distance = Phaser.Math.distance(this.x, this.y, base.x, base.y);
			if (distance < 20) {

				this.destroy();
			}
			else if (distance < 200) {
				var angle = Phaser.Math.angleBetweenPoints(this.position, base.position);
				var speed = 10000;
				this.body.force.x = Math.cos(angle) * speed;
				this.body.force.y = Math.sin(angle) * speed;

				var scale = Math.abs(distance / 200);

				this.scale.x = scale;
				this.scale.y = scale;
			}
		}, this);
	};

	return Asteroid;

});
