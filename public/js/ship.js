// js/ship.js

define([
	'phaser',
	'GS'
], function(Phaser, GS) {

	var Ship = function(game, x, y) {
		Phaser.Sprite.call(this, game, x, y, 'sprites', 'playerShip2_blue.png');
		this.anchor.set(0.5);

		this.flame = new Phaser.Image(game, 0, 40, 'sprites', 'fire01.png');
		this.flame.anchor.set(0.5, 1);
		this.flame.angle = 180;
		this.flame.visible = false;
		this.addChild(this.flame);

		game.physics.p2.enable(this, false);		// Set second parameter to "true" to debug physics body

		this.body.clearShapes();
		this.body.loadPolygon('sprite_physics', 'playerShip2');
		this.body.mass = 20;

		this.speed = 3;
		this.health = 10;
	};

	Ship.prototype = Object.create(Phaser.Sprite.prototype);

	Ship.prototype.constructor = Ship;

	return Ship;

});
